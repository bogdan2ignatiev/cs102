from typing import List, Tuple, TypeVar, Optional, Set

BLOCK_SIDE_LEN = 3
SEQ_LEN = 9
EMPTY_POS = "."

Grid = List[List[str]]
Pos = Tuple[int, int]
T = TypeVar("T")

def read_sudoku(filename: str) -> Grid:
    """ Прочитать Судоку из указанного файла """
    digits = [c for c in open(filename).read() if c in '123456789.']
    grid = group(digits, 9)
    return grid


def display(values: Grid):
    """Вывод Судоку """
    width = 2
    line = '+'.join(['-' * (width * 3)] * 3)
    for row in range(9):
        print(''.join(values[row][col].center(width) + ('|' if str(col) in '25' else '') for col in range(9)))
        if str(row) in '25':
            print(line)
    print()


def group(values: List[T], n: int) -> List[List[T]]:
    """
    Сгруппировать значения values в список, состоящий из списков по n элементов

    >>> group([1,2,3,4], 2)
    [[1, 2], [3, 4]]
    >>> group([1,2,3,4,5,6,7,8,9], 3)
    [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
    """
    return [values[i:i+n] for i in range(0, len(values), n)]


def get_row(values: Grid, pos: Pos) -> List[str]:
    """ Возвращает все значения для номера строки, указанной в pos

    >>> get_row([['1', '2', '.'], ['4', '5', '6'], ['7', '8', '9']], (0, 0))
    ['1', '2', '.']
    >>> get_row([['1', '2', '3'], ['4', '.', '6'], ['7', '8', '9']], (1, 0))
    ['4', '.', '6']
    >>> get_row([['1', '2', '3'], ['4', '5', '6'], ['.', '8', '9']], (2, 0))
    ['.', '8', '9']
    """
    row_index = pos[0]
    return values[row_index]


def get_col(values: Grid, pos: Pos) -> List[str]:
    """ Возвращает все значения для номера столбца, указанного в pos

    >>> get_col([['1', '2', '.'], ['4', '5', '6'], ['7', '8', '9']], (0, 0))
    ['1', '4', '7']
    >>> get_col([['1', '2', '3'], ['4', '.', '6'], ['7', '8', '9']], (0, 1))
    ['2', '.', '8']
    >>> get_col([['1', '2', '3'], ['4', '5', '6'], ['.', '8', '9']], (0, 2))
    ['3', '6', '9']
    """
    _, ci = pos
    return [row[ci] for row in values]


def get_block(values: Grid, pos: Pos) -> List[str]:
    """ Возвращает все значения из квадрата, в который попадает позиция pos

    >>> grid = read_sudoku('puzzle1.txt')
    >>> get_block(grid, (0, 1))
    ['5', '3', '.', '6', '.', '.', '.', '9', '8']
    >>> get_block(grid, (4, 7))
    ['.', '.', '3', '.', '.', '1', '.', '.', '6']
    >>> get_block(grid, (8, 8))
    ['2', '8', '.', '.', '.', '5', '.', '7', '9']
    """
    pos_block_vert_index = int(pos[0] / 3)
    pos_block_hor_index = int(pos[1] / 3)
    block = []

    for row_index in range(pos_block_vert_index * BLOCK_SIDE_LEN, (pos_block_vert_index + 1) * BLOCK_SIDE_LEN):
        for col_index in range(pos_block_hor_index * BLOCK_SIDE_LEN, (pos_block_hor_index + 1) * BLOCK_SIDE_LEN):
            block.append(values[row_index][col_index])

    return block


def find_empty_positions(grid: Grid) -> Optional[Pos]:
    """ Найти первую свободную позицию в пазле

    >>> find_empty_positions([['1', '2', '.'], ['4', '5', '6'], ['7', '8', '9']])
    (0, 2)
    >>> find_empty_positions([['1', '2', '3'], ['4', '.', '6'], ['7', '8', '9']])
    (1, 1)
    >>> find_empty_positions([['1', '2', '3'], ['4', '5', '6'], ['.', '8', '9']])
    (2, 0)
    """
    for row_index in range(0, len(grid)):
        for col_index in range(0, len(grid[row_index])):
            if grid[row_index][col_index] == EMPTY_POS:
                return (row_index, col_index)
    return None


def find_possible_values(grid: Grid, pos: Pos) -> Set[str]:
    """ Вернуть множество возможных значения для указанной позиции

    >>> grid = read_sudoku('puzzle1.txt')
    >>> values = find_possible_values(grid, (0,2))
    >>> values == {'1', '2', '4'}
    True
    >>> values = find_possible_values(grid, (4,7))
    >>> values == {'2', '5', '9'}
    True
    """
    possible_values = set("123456789")
    possible_values -= set(get_row(grid, pos))
    possible_values -= set(get_col(grid, pos))
    possible_values -= set(get_block(grid, pos))

    return possible_values


def solve(grid: Grid) -> Optional[Grid]:
    """ Решение пазла, заданного в grid """
    """ Как решать Судоку?
        1. Найти свободную позицию
        2. Найти все возможные значения, которые могут находиться на этой позиции
        3. Для каждого возможного значения:
            3.1. Поместить это значение на эту позицию
            3.2. Продолжить решать оставшуюся часть пазла

    >>> grid = read_sudoku('puzzle1.txt')
    >>> solve(grid)
    [['5', '3', '4', '6', '7', '8', '9', '1', '2'], ['6', '7', '2', '1', '9', '5', '3', '4', '8'], ['1', '9', '8', '3', '4', '2', '5', '6', '7'], ['8', '5', '9', '7', '6', '1', '4', '2', '3'], ['4', '2', '6', '8', '5', '3', '7', '9', '1'], ['7', '1', '3', '9', '2', '4', '8', '5', '6'], ['9', '6', '1', '5', '3', '7', '2', '8', '4'], ['2', '8', '7', '4', '1', '9', '6', '3', '5'], ['3', '4', '5', '2', '8', '6', '1', '7', '9']]
    """
    first_empty_pos = find_empty_positions(grid)

    if first_empty_pos == None:
        return grid # Нет свободных позиций - значит решать больше нечего

    possible_values = find_possible_values(grid, first_empty_pos)

    if len(possible_values) == 0:
            return None

    for value in possible_values:
        grid[first_empty_pos[0]][first_empty_pos[1]] = value # 

        if solve(grid) != None:
            return solve(grid)
    
    grid[first_empty_pos[0]][first_empty_pos[1]] = EMPTY_POS # Если ничего подставить не получилось, то возвращаемся к прошлому состоянию
    return None 


def check_solution(solution: Grid) -> bool:
    """ Если решение solution верно, то вернуть True, в противном случае False """
    # TODO: Add doctests with bad puzzles
    def check_seq(seq: List[str]) -> bool:
        entries = dict.fromkeys(["1", "2", "3", "4", "5", "6", "7", "8", "9"], 0)
        
        for value in seq:
            entries[value] += 1
            
            if entries[value] > 1:
                return False
        
        return True


    for row in solution:
        if not check_seq(row):
            return False

    for col_index in range(0, SEQ_LEN):
        first_pos_in_col = (0, col_index)
        col = get_col(solution, first_pos_in_col)

        if not check_seq(col):
            return False

    for block_vert_index in range(0, SEQ_LEN, BLOCK_SIDE_LEN):
        for block_hor_index in range(0, SEQ_LEN, BLOCK_SIDE_LEN):
            first_pos_in_block = (block_vert_index, block_hor_index)
            block = get_block(solution, first_pos_in_block)

            if not check_seq(block):
                return False

    return True


def generate_sudoku(N: int) -> Grid:
    """ Генерация судоку заполненного на N элементов

    >>> grid = generate_sudoku(40)
    >>> sum(1 for row in grid for e in row if e == '.')
    41
    >>> solution = solve(grid)
    >>> check_solution(solution)
    True
    >>> grid = generate_sudoku(1000)
    >>> sum(1 for row in grid for e in row if e == '.')
    0
    >>> solution = solve(grid)
    >>> check_solution(solution)
    True
    >>> grid = generate_sudoku(0)
    >>> sum(1 for row in grid for e in row if e == '.')
    81
    >>> solution = solve(grid)
    >>> check_solution(solution)
    True
    """
    sudoku = []

    for _ in range(0, SEQ_LEN): # Заполняем точками все поле
        row: List[str] = []
        sudoku.append(row)

        for _ in range(0, SEQ_LEN):
            row.append(EMPTY_POS)

    sudoku = solve(sudoku)

    start_row_index = int(N / SEQ_LEN)

    for row_index in range(start_row_index, SEQ_LEN): # Заполняем точками то, что не нужно было заполнять цифрами
        start_col_index = N % SEQ_LEN if row_index == start_row_index else 0

        for col_index in range(start_col_index, SEQ_LEN):
            sudoku[row_index][col_index] = EMPTY_POS

    return sudoku


if __name__ == '__main__':
    for fname in ['puzzle1.txt', 'puzzle2.txt', 'puzzle3.txt']:
        grid = read_sudoku(fname)
        display(grid)
        solution = solve(grid)
        display(solution)
